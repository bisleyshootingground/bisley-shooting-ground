There is nowhere quite like Bisley Shooting Ground. Set in over 3000 acres of natural woodland, Bisley blends old charm with world-renowned facilities. Only 30 miles from central London, Bisley Shooting Ground near Woking in Surrey, is described by many as the country's top Clay Shooting ground for Tuition, Practice and Corporate Events, with industry leading coaching.

Website : https://www.bisleyshooting.com/
